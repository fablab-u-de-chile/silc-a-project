# SILC-A

The SILC-A is an automated machine for impact testing on rocks. It is based on the [SILC machine](https://www.researchgate.net/publication/45363104_A_portable_load_cell_for_in-situ_ore_impact_breakage_testing)(short impact load cell), its previous version with less automated features.

<img src="/imgs/silc.png" width="500">

	Figure 1: CAD and actual machine.

The machine breaks a rock sample dropping a steel ball on it. The sample is on top of a steel bar equipped with a load cell, the deformation on the bar induced by the impact is used to estimate breaking energy of the rock sample.

This repo contains:

- Links for the repo of every part of the project.
- Misc info.
- User guide.

Repos:

- [Mechanical](https://gitlab.com/fablab-u-de-chile/silc_mechanical)
- [PCBs](https://gitlab.com/fablab-u-de-chile/silc_pcbs)
- [GUI](https://gitlab.com/fablab-u-de-chile/silc_gui)
- [firmware](https://gitlab.com/fablab-u-de-chile/silc)




